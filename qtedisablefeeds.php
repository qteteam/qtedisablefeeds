<?php
/*
 * Plugin Name: QTE Disable Feeds
 * Description: Disables wp feeds
 * Version: 0.1.0
 * Author: QTE Development AB
 * Author URI: https://getqte.se/
 */

 if( ! defined('ABSPATH')) {
     exit;
 }

 define('QTEDISABLEFEEDS_VERSION', '0.1.0');

define( 'QTEDISABLEFEEDS_PLUGIN_DIR', plugin_dir_url( __FILE__ ) );
define( 'QTEDISABLEFEEDS_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );


require_once QTEDISABLEFEEDS_PLUGIN_PATH . 'includes/feeds.php';

