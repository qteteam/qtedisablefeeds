<?php 
namespace QTEDISABLEFEEDS\Feeds;

if( ! defined('ABSPATH')) {
    exit;
}

function disable_feed() {
wp_die( __('No feed available,please visit our <a href="'. get_bloginfo('url') .'">homepage</a>!') );
}

add_action('do_feed', __NAMESPACE__ . '\\disable_feed', 1);
add_action('do_feed_rdf', __NAMESPACE__ . '\\disable_feed', 1);
add_action('do_feed_rss', __NAMESPACE__ . '\\disable_feed', 1);
add_action('do_feed_rss2', __NAMESPACE__ . '\\disable_feed', 1);
add_action('do_feed_atom', __NAMESPACE__ . '\\disable_feed', 1);
add_action('do_feed_rss2_comments', __NAMESPACE__ . '\\disable_feed', 1);
add_action('do_feed_atom_comments', __NAMESPACE__ . '\\disable_feed', 1);
 ?>